//A kód forrása: https://progpater.blog.hu/2011/02/13/bearazzuk_a_masodik_labort


#include <stdio.h>
#include <math.h>
#include <stdlib.h>


 void kiir (double tomb[], int db)
 {
  for(int i=0;i<db;i++)
  {
   printf("PageRank %d: %lf\n", i, tomb[i]);
  }
 }

 double tavolsag (double PR[], double PRv[], int n)
 {
  double sum=0.0;
  for(int i=0;i<n;++i)
  {
   sum+= (PR[i]-PRv[i])*(PR[i]-PRv[i]);
  }
  return sqrt (sum);
 }

int main(void){

 double L[4][4] = {
       // J   JP   JPL    M
        {0.0,0.0,1.0/3.0,0.0}, 		//J
        {1.0,1.0/2.0,1.0/3.0,1.0},	//JP
        {0.0,1.0/2.0,0.0,0.0},		//JPL
        {0.0,0.0,1.0/3.0,0.0}		//M
                            };

 double PR[4] = {0.0,0.0,0.0,0.0};
 double PRv[4] = {1.0/4.0,1.0/4.0,1.0/4.0,1.0/4.0};
 
 for(;;)
 {
  for(int i=0; i<4; i++)
  {
   PR[i] = PRv[i];
  }
  for (int i=0; i<4; i++)
   {
   double seged = 0.0;
   for (int j=0; j<4; j++)
   {
    seged+= L[i][j] * PR[j];
    PRv[i] = seged;
   }
  }
 
   

  if(tavolsag(PR, PRv, 4) < 0.000001)
  {
   break;
  }
 }

 kiir (PR, 4);
 return 0;
}

